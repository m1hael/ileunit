      //
      // Prototype for EXTTST.
      //

     D activateTestSuite...
     D                 pr                  ExtProc('activateTestSuite')
     D  testSuite                          LikeDs(TestSuite_t)
     D  actMark                            Const Like(ActMark_t)

     D getTestProc     pr                  LikeDs(Proc_t)
     D                                     ExtProc('getTestProc')
     D  testSuite                          Const LikeDs(TestSuite_t)
     D  testIdx                      10i 0 Const

     D getTestSuite    pr                  LikeDs(TestSuite_t)
     D                                     ExtProc('getTestSuite')
     D  procNmList                         Const LikeDs(ProcNmList_t)

     D getTestNm       PR                  like(procnm_t)
     D                                     extproc('getTestNm')
     D  testSuite                          const likeds(testsuite_t)
     D  testIndex                    10I 0 const