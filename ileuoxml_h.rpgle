      /if not defined (ILEUNIT_OUT_XML)
      /define ILEUNIT_OUT_XML
      
      *-------------------------------------------------------------------------
      * Prototypes
      *-------------------------------------------------------------------------
     D ileunit_writer_xml_create...
     D                 PR              *   extproc('ileunit_writer_xml_create')
     D   ifsFilePath               1024A   const varying
      *
     D ileunit_writer_xml_output...
     D                 PR                  extproc('ileunit_writer_xml_output')
     D   writer                        *   const
     D   testSuite                   50A   const
     D   testResults                   *   const
      *
     D ileunit_writer_xml_finalize...
     D                 PR                  extproc('ileunit_writer_xml_-
     D                                     finalize')
     D   writer                        *

      /endif
      