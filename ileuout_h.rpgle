      /if not defined (ILEUOUT_H)
      /define ILEUOUT_H

      *-------------------------------------------------------------------------
      * Prototypes
      *-------------------------------------------------------------------------
     D ileunit_writer_outputTestResults...
     D                 PR                  extproc('ileunit_writer_-
     D                                     outputTestResults')
     D   writer                        *   const
     D   testSuite                   50A   const
     D   testResults                   *   const
      *
     D ileunit_writer_finalize...
     D                 PR                  extproc('ileunit_writer_finalize')
     D   writer                        *

      /endif

