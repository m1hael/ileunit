     /**
      * \brief ILEUnit Writer : XML
      *
      *
      * \author Mihael Schmidt
      * \date   09.02.2011
      *
      * \todo system-out and system-err output
      * \todo classname => module path ( lib/srvpgm[(module)] )
      * \todo properties?
      */


     H nomain


      *-------------------------------------------------------------------------
      * Prototypen
      *-------------------------------------------------------------------------
     D calculateResultSums...
     D                 PR                  likeds(sum_result_t)
     D   testResults                   *   const
      *
     D getXmlFileHandle...
     D                 PR            10I 0
     D   filePath                  1024A   const
      *
     D writeHeader     PR
     D   fileHandle                  10I 0 const
     D   testSuiteName...
     D                               50A   const
     D   testResults                   *   const
      *
     D writeFooter     PR
     D   fileHandle                  10I 0 const
      *
     D writeProperties...
     D                 PR
     D   fileHandle                  10I 0 const
      *
     D writeTestResults...
     D                 PR
     D   fileHandle                  10I 0 const
     D   testSuiteName...
     D                               50A   const
     D   testResults                   *   const
      *
      /include 'ileuoxml_h.rpgle'
      /include 'llist/llist_h.rpgle'
      /include 'ifsio_h.rpgle'
      /include 'errno_h.rpgle'
      /include 'message_h.rpgle'
      /include 'libc_h.rpgle'


      *-------------------------------------------------------------------------
      * Templates
      *-------------------------------------------------------------------------
     D userdata_t      DS                  qualified template
     D   path                      1024A
      *
     D sum_result_t    DS                  qualified template
     D   assert                      10I 0
     D   failure                     10I 0
     D   error                       10I 0
     D   test                        10I 0

      /include 'ileuout_t.rpgle'
      /include 'ileunit_t.rpgle'


      *-------------------------------------------------------------------------
      * Constants
      *-------------------------------------------------------------------------
     D ILEUNIT_WRITER_XML_ID...
     D                 C                   'ileunit_writer_xml'
     D CRLF            C                   x'0d25'
     D TAB             C                   '    '


      *-------------------------------------------------------------------------
      * Procedures
      *-------------------------------------------------------------------------


     P ileunit_writer_xml_create...
     P                 B                   export
     D                 PI              *
     D   ifsFilePath               1024A   const varying
      *
     D header          DS                  likeds(ileunit_writer_t)
     D                                     based(writer)
     D userdata        DS                  likeds(userdata_t)
     D                                     based(header.userdata)
      /free
       writer = %alloc(%size(ileunit_writer_t));

       header.id = ILEUNIT_WRITER_XML_ID;
       header.userdata = %alloc(%size(userdata_t));
       header.proc_output = %paddr('ileunit_writer_xml_output');
       header.proc_finalize = %paddr('ileunit_writer_xml_finalize');
       userdata.path = ifsFilePath;

       return writer;
      /end-free
     P                 E


     P ileunit_writer_xml_output...
     P                 B                   export
     D                 PI
     D   writer                        *   const
     D   testSuite                   50A   const
     D   testResults                   *   const
      *
     D header          DS                  likeds(ileunit_writer_t)
     D                                     based(writer)
     D udata           DS                  likeds(userdata_t)
     D                                     based(header.userdata)
      *
     D fileHandle      S             10I 0
      /free
       fileHandle = getXmlFileHandle(udata.path);

       writeHeader(fileHandle : testSuite : testResults);
       writeProperties(fileHandle);
       writeTestResults(fileHandle : testSuite : testResults);
       writeFooter(fileHandle);

       callp close(fileHandle);
      /end-free
     P                 E


     P ileunit_writer_xml_finalize...
     P                 B                   export
     D                 PI
     D   writer                        *
      *
     D header          DS                  likeds(ileunit_writer_t)
     D                                     based(writer)
     D userdata        DS                  likeds(userdata_t)
     D                                     based(header.userdata)
      /free
       if (writer <> *null);

         if (header.userdata <> *null);
           dealloc header.userdata;
         endif;

         dealloc(n) writer;
       endif;
      /end-free
     P                 E


     P calculateResultSums...
     P                 B
     D                 PI                  likeds(sum_result_t)
     D   testResults                   *   const
      *
     D result          DS                  likeds(sum_result_t)
     D testResult      DS                  likeds(TestResult_t) based(ptr)
      /free
       clear result;

       list_abortIteration(testResults);

       ptr = list_getNext(testResults);
       dow (ptr <> *null);

         result.assert += testResult.assertCount;
         result.failure += testResult.failureCount;
         result.error += testResult.errorCount;
         result.test += 1;

         ptr = list_getNext(testResults);
       enddo;

       return result;
      /end-free
     P                 E


     P getXmlFileHandle...
     P                 B
     D                 PI            10I 0
     D   filePath                  1024A   const
      *
     D fileHandle      S             10I 0
     D flags           S             10I 0
     D mode            S             10I 0
     D errorNumber     S             10I 0 based(errorPtr)
      /free
       if (access(%trimr(filePath) : F_OK) >= 0);
         // delete existing file
         unlink(%trimr(filePath));
       endif;

       // create file
       flags = O_WRONLY + O_CREAT + O_TRUNC + O_CCSID +
               O_TEXTDATA + O_TEXT_CREAT;
       mode =  S_IRUSR + S_IWUSR + S_IRGRP;
       fileHandle = open(%trimr(filePath) : flags : mode : 819 : 0);
       if (fileHandle < 0);
         // could not open file handle
         errorPtr = errno();
         sndEscapeMsg('Could not create file ' + %trimr(filePath) +
                      ': ' + %str(strerror(errorNumber)) : 
                      TWO_CALL_STK_LVL_ABOVE);
       endif;

       return fileHandle;
      /end-free
     P                 E


     P writeHeader     B
     D                 PI
     D   fileHandle                  10I 0 const
     D   testSuiteName...
     D                               50A   const
     D   testResults                   *   const
      *
     D sumResult       DS                  likeds(sum_result_t)
     D text            S           1000A
      /free
       sumResult = calculateResultSums(testResults);
       // TODO get hostname instead of localhost
       text = '<?xml version="1.0" encoding="UTF-8" ?>' + CRLF +
              '<testsuite errors="' + %char(sumResult.error) + '" ' +
              'failures="' + %char(sumResult.failure) + '" ' +
              'hostname="localhost" ' +
              'name="' + %trimr(testSuiteName) + '" ' +
              'tests="' + %char(list_size(testResults)) + '" >' + CRLF;

       writeA(fileHandle : text : %len(%trimr(text)));
      /end-free
     P                 E


     P writeTestResults...
     P                 B
     D                 PI
     D   fileHandle                  10I 0 const
     D   testSuiteName...
     D                               50A   const
     D   testResults                   *   const
      *
     D testResult      DS                  likeds(TestResult_t) based(ptr)
     D text            S           1000A
      /free
       list_abortIteration(testResults);
       ptr = list_getNext(testResults);
       dow (ptr <> *null);

         text = TAB + '<testcase name="' + %trimr(testResult.testName) + '">' +
                CRLF +
                TAB + '</testcase>' + CRLF;

         writeA(fileHandle : text : %len(%trimr(text)));

         ptr = list_getNext(testResults);
       enddo;
      /end-free
     P                 E


     P writeProperties...
     P                 B
     D                 PI
     D  fileHandle                   10I 0 const
      /free

      /end-free
     P                 E


     P writeFooter     B
     D                 PI
     D   fileHandle                  10I 0 const
      *
     D text            S           1000A
      /free
       text = '</testsuite>';
       writeA(fileHandle : text : %len(%trimr(text)));
      /end-free
     P                 E
