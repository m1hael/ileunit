      /if not defined (ILEUNIT_OUT_USERSPACE)
      /define ILEUNIT_OUT_USERSPACE

      *-------------------------------------------------------------------------
      * Prototypes
      *-------------------------------------------------------------------------
     D ileunit_writer_userspace_create...
     D                 PR              *   extproc('ileunit_writer_userspace_-
     D                                     create')
     D   userspaceLibrary...
     D                               10A   const
     D   userspaceName...
     D                               10A   const
      *
     D ileunit_writer_userspace_output...
     D                 PR                  extproc('ileunit_writer_userspace_-
     D                                     output')
     D   writer                        *   const
     D   testSuite                   50A   const
     D   testResults                   *   const
      *
     D ileunit_writer_userspace_finalize...
     D                 PR                  extproc('ileunit_writer_userspace_-
     D                                     finalize')
     D   writer                        *

      /endif
      