
     H nomain

      *-------------------------------------------------------------------------
      * Prototypes
      *-------------------------------------------------------------------------
      /include 'error_h.rpgle'
      /include 'message_h.rpgle'


       //----------------------------------------------------------------------
       // Raise an ILEUnit error. See prototype.
       //----------------------------------------------------------------------
     P raiseRUError    B                   export
     D                 PI
     D  message                     256A   const varying
      /free

        sndEscapeMsgAboveCtlBdy( 'ILEUnit Error. ' + message);

      /end-free
     P                 E
