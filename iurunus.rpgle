     /**
     * \brief ILEUnit : Run test suite with output to a user space
   *
   * \author Mihael Schmidt
   * \date   25.08.2012
      * 
      * \param Userspace Library
      * \param Userspace Name
      * \param Testsuite Library
      * \param Testsuite Name
   */

     D IU_RUN_US       PR                  extpgm('IURUNUS')
     D   uslib                       10A   const
     D   usname                      10A   const
     D   testlib                     10A   const
     D   testsuite                   10A   const
     D   returnValue                 10I 0
     D IU_RUN_US       PI
     D   uslib                       10A   const
     D   usname                      10A   const
     D   testlib                     10A   const
     D   testsuite                   10A   const
     D   returnValue                 10I 0
     
      *-------------------------------------------------------------------------
      * Prototypen
      *-------------------------------------------------------------------------
      /include 'ileuous_h.rpgle'
      /include 'runrmt_h.rpgle'
       
     D main            PR 
     D   uslib                       10A   const
     D   usname                      10A   const
     D   testlib                     10A   const
     D   testsuite                   10A   const
     D   returnValue                 10I 0
       
      /free
       main(uslib : usname : testlib : testsuite : returnValue);
       
       *inlr = *on;
      /end-free
      
     
     P main            B
     D                 PI
     D   uslib                       10A   const
     D   usname                      10A   const
     D   testlib                     10A   const
     D   testsuite                   10A   const
     D   returnValue                 10I 0
      *
     D writer          S               *
      /free
       writer = ileunit_writer_userspace_create(uslib : usname);
       
       returnValue = ileunit_runTestSuite(writer : testsuite : testlib);
       
       ileunit_writer_userspace_finalize(writer);
      /end-free
     P                 E
      