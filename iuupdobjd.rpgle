     /**
      * \brief Tag unit test objects
      *
      * Unit test objects (service programs) are tagged as such. The
      * object description attribute is changed to "ILEUNIT".
      *
      * \author Mihael Schmidt
      * \date 2010-12-05
      */

      /if not defined(QUSEC)
      /define QUSEC
      /copy QSYSINC/QRPGLESRC,QUSEC
      /endif


      *-------------------------------------------------------------------------
      * PEP
      *-------------------------------------------------------------------------
     D IUUPDOBJD       PR                  EXTPGM('IUUPDOBJD')
     D   library                     10A   const
     D   object                      10A   const
     D IUUPDOBJD       PI
     D   library                     10A   const
     D   object                      10A   const


      *-------------------------------------------------------------------------
      * Prototypes
      *-------------------------------------------------------------------------
     D changeObjectDesc...
     D                 PR                  extpgm('QLICOBJD')
     D   returnedLibraryName...
     D                               10A
     D   qualObjectName...
     D                               20A   const
     D   objectType                  10A   const
     D   changeObjectInfo...
     D                            32767A   options(*varsize)
     D   error                             likeds(QUSEC)
      *
     D main            PR
     D   library                     10A   const
     D   object                      10A   const

      /free
       if (%parms() = 2);
         main(library : object);
       endif;

       *inlr = *on;
       return;
      /end-free


     /**
      * \brief Main procedure
      *
      * Main procedure for eliminating global resources.
      */
     P main            B
     D                 PI
     D   library                     10A   const
     D   object                      10A   const
      *
     D lib             S             10A
     D objectInfo      DS                  qualified
     D   keys                        10I 0
     D   type1                       10I 0
     D   length1                     10I 0
     D   attribute                   10A

      /free
       objectInfo.keys = 1;
       objectInfo.type1 = 9;
       objectInfo.length1 = 10;
       objectInfo.attribute = 'ILEUNIT';

       clear QUSEC;
       QUSBPRV = 0;

       changeObjectDesc(lib : object + library : '*SRVPGM' :
                        objectInfo : QUSEC);
      /end-free
     P                 E

