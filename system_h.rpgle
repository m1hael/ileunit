      /if not defined (ILEUNIT_SYSTEM)
      /define ILEUNIT_SYSTEM

      // Prototype for API QWVRCSTK (Retrieve Call Stack).

     DQWVRCSTK         pr                  ExtPgm('QWVRCSTK')
     D rawCallStk                 32766a   Options(*VarSize)
     D rawCallStkLen                 10i 0 Const
     D fmtRcvInfo                     8a   Const
     D jobIdInfo                           Const LikeDs(dsJIDF0100)
     D fmtJobIdInfo                   8a   Const
     D errors                     32766a   Options(*VarSize) NoOpt

     D dsJIDF0100      ds                  Qualified Based(template)
     D  jobNm                        10a
     D  usrNm                        10a
     D  jobNb                         6a
     D  intJobId                     16a
     D  reserved                      2a
     D  threadInd                    10i 0
     D  threadId                      8a

     D dsCSTK0100Hdr   ds                  Qualified Based(template)
     D  bytesP                       10i 0
     D  bytesA                       10i 0
     D  nbCallStkEntForThread...
     D                               10i 0
     D  callStkOff                   10i 0
     D  nbCallStkEntRtn...
     D                               10i 0
     D  threadId                      8a
     D  infoSts                       1a

     D dsCSTK0100Ent   ds                  Qualified Based(template)
     D  len                          10i 0
     D  dsplToSttId                  10i 0
     D  nbSttId                      10i 0
     D  dsplToProcNm                 10i 0
     D  procNmLen                    10i 0
     D  rqsLvl                       10i 0
     D  pgmNm                        10a
     D  pgmLibNm                     10a
     D  miInstNb                     10i 0
     D  modNm                        10a
     D  modLibNm                     10a
     D  ctlBndry                      1a
     D                                3a
     D  actGrpNb                     10u 0
     D  actGrpNm                     10a
     D                                2a
     D  pgmAspNm                     10a
     D  pgmLibAspNm                  10a
     D  pgmAspNb                     10i 0
     D  pgmLibAspNb                  10i 0


      //
      // Receive Program Message API prototype.
      //
      // http://publib.boulder.ibm.com/iseries/v5r2/ic2924/index.htm?info/apis/QMHRCVPM.HTM
      //

     D QMHRCVPM        pr                  ExtPgm('QMHRCVPM')
     D  msgInfo                   32565a   Options(*VarSize)
     D  msgInfoLen                   10i 0 Const
     D  fmtNm                         8a   Const
     D  callStkEnt                   10a   Const
     D  callStkCnt                   10i 0 Const
     D  msgType                      10a   Const
     D  msgKey                        4a   Const
     D  waitTime                     10i 0 Const
     D  msgAction                    10a   Const
     D  errorCode                 32565a   Options(*VarSize) NoOpt

     D RCVM0200Hdr     ds                  Qualified Based(template)
     D  bytesR                       10i 0
     D  bytesA                       10i 0
     D  msgSev                       10i 0
     D  msgId                         7a
     D  msgType                       2a
     D  msgKey                        4a
     D  msgFileNm                    10a
     D  msgFileLibS                  10a
     D  msgFileLibU                  10a
     D  sendingJob                   10a
     D  sendingUsr                   10a
     D  sendingJobNb                  6a
     D  sendingPgmNm                 12a
     D  sendingPgmSttNb...
     D                                4a
     D  dateSent                      7a
     D  timeSent                      6a
     D  rcvPgmNm                     10a
     D  rcvPgmSttNb                   4a
     D  sendingType                   1a
     D  rcvType                       1a
     D                                1a
     D  CCSIDCnvStsIndForTxt...
     D                               10i 0
     D  CCSIDCnvStsIndForData...
     D                               10i 0
     D  alertOpt                      9a
     D  CCSIDMsgAndMsgHlp...
     D                               10i 0
     D  CCSIDRplData                 10i 0
     D  rplDataLenR                  10i 0
     D  rplDataLenA                  10i 0
     D  msgLenR                      10i 0
     D  msgLenA                      10i 0
     D  msgHlpLenR                   10i 0
     D  msgHlpLenA                   10i 0

     D RCVM0300Hdr     ds                  Qualified Based(template)
     D  bytesR                       10i 0
     D  bytesA                       10i 0
     D  msgSev                       10i 0
     D  msgId                         7a
     D  msgType                       2a
     D  msgKey                        4a
     D  msgFileNm                    10a
     D  msgFileLibS                  10a
     D  msgFileLibU                  10a
     D  alertOpt                      9a
     D  CCSIDCnvStsIndOfMsg...
     D                               10i 0
     D  CCSIDCnvStsIndForData...
     D                               10i 0
     D  CCSIDRplData                 10i 0
     D  CCSIDRplDataMsgHlp...
     D                               10i 0
     D  rplDataLenR                  10i 0
     D  rplDataLenA                  10i 0
     D  msgLenR                      10i 0
     D  msgLenA                      10i 0
     D  msgHlpLenR                   10i 0
     D  msgHlpLenA                   10i 0
     D  sndInfoLenR                  10i 0
     D  sndInfoLenA                  10i 0

     D RCVM0300Sender  ds                  Qualified Based(template)
     D  sndJob                       10a
     D  sndUsrPrf                    10a
     D  sndJobNb                      6a
     D  dateSent                      7a
     D  timeSent                      6a
     D  sndType                       1a
     D  rcvType                       1a
     D  sndPgmNm                     12a
     D  sndModNm                     10a
     D  sndProcNm                   256a
     D                                1a
     D  sndPgmSttCnt                 10i 0
     D  sndPgmSttNb                  30a
     D  rcvPgmNm                     10a
     D  rcvModNm                     10a
     D  rcvProcNm                   256a
     D                               10a
     D  rcvPgmSttCnt                 10i 0
     D  rcvPgmSttNb                  30a
     D                                2a
      // ...last fields omitted.


      //----------------------------------------------------------------------
      //   Symbolic Constants
      //----------------------------------------------------------------------

       // Format Name:
       // - most detailed format for QMHRCVPM API.
     D ALL_MSG_INFO_WITH_SENDER_INFO...
     D                 c                   Const('RCVM0300')

       // The current call stack entry.
     D THIS_CALL_STK_ENT...
     D                 c                   Const('*')

       // No message key.
     D NO_MSG_KEY      c                   Const(*blank)

       // Do not wait for receiving the message.
     D NO_WAIT         c                   Const(0)

       // Message action:
       // - keep the message in the message queue and mark it as an old message
     D MARK_AS_OLD     c                   Const('*OLD')
       // - remove the message after receiving it
     D REMOVE_MSG      c                   Const('*REMOVE')


      //
      // Send Program Message API
      //

     D QMHSNDPM        PR                  ExtPgm('QMHSNDPM')
     D   msgID                        7a   Const
     D   qlfMsgF                     20a   Const
         // TODO Add Options(*VarSize to msgData).
     D   msgData                    256a   Const
     D   msgDataLen                  10i 0 Const
     D   msgType                     10a   Const
     D   callStkEnt                  10a   Const
     D   callStkCnt                  10i 0 Const
     D   msgKey                       4a
     D   error                     1024a   Options(*VarSize) NoOpt



       // List ILE service program information API
     D QBNLSPGM        pr                  ExtPgm('QBNLSPGM')
     D   usrSpc                      20a   Const
     D   fmt                          8a   Const
     D   srvPgm                      20a   Const
     D   errors                   32766a   Options(*VarSize) NoOpt

       // Structure of an entry in a QBNLSPGM list.
     D dsSPGL0610      ds                  Qualified Based(template)
        // Size of this entry. Each entry has a different size.
     D  size                         10i 0
     D  srvPgm                       20a
     D  ccsid                        10i 0
        // Memory offset to reach the procedure name.
     D  procNmOff                    10i 0
        // Longueur du nom de la procedure.
     D  procNmSize                   10i 0


       // List API generic header structure (template).
     D ListHdr_t       ds                   Qualified Based(template)
         // Filler
     D   filler1                    103a
         // Status (I=Incomplete,C=Complete,F=Partially Complete)
     D   status                       1a
         // Filler
     D   filler2                     12a
         // Header Offset
     D   hdrOff                      10i 0
         // Header Size
     D   hdrSize                     10i 0
         // List Offset
     D   listOff                     10i 0
         // List Size
     D   listSize                    10i 0
         // Count of Entries in List
     D   entCnt                      10i 0
         // Size of a single entry
     D   entSize                     10i 0


      //
      // Prototype for QleActBndPgm API.
      //
      // "Activate Bound Program".
      //
      // http://publib.boulder.ibm.com/iseries/v5r2/ic2924/info/apis/qleactbp.htm
      //

      // (RETURN) Activation mark.
     DQleActBndPgm     pr            10i 0 ExtProc('QleActBndPgm')
       // (INPUT) Pointer to bound program.
     D srvPgm_p                        *   Const ProcPtr
       // (OUTPUT) Activation mark.
     D actMark                       10i 0 Options(*Omit)
       // (OUTPUT) Activation information.
     D actInfo                       64a   Options(*Omit)
       // (INPUT) Length of activation information.
     D actInfoLen                    10i 0 Const Options(*Omit)
       // (I/O) Error code.
     D errors                     32767a   Options(*VarSize: *Omit) NoOpt


      //
      // Prototype for RslvSP MI instruction.
      //
      // Resolve System Pointer.
      //

     D rslvSP          pr              *   ExtProc('rslvsp') ProcPtr
        // Object type (in hexadecimal format -- see QLICVTTP API).
     D  hexType                       2a   Value
        // Object name.
     D  object                         *   Value Options(*String)
        // Library name.
     D  lib                            *   Value Options(*String)
        // Authority.
     D  auth                          2a   Value


      //
      // Prototype for QleGetExp API.
      //
      // Get export pointer.
      //
      // http://publib.boulder.ibm.com/iseries/v5r2/ic2924/info/apis/qlegetexp.htm
      //

     DQleGetExp        pr              *   ExtProc('QleGetExp') ProcPtr
     D actMark                       10i 0 Const Options(*Omit)
     D expNo                         10i 0 Const Options(*Omit)
     D expNmLen                      10i 0 Const Options(*Omit)
     D expNm                      32767a   Const Options(*VarSize: *Omit)
     D expPtr                          *   Options(*Omit) ProcPtr
     D expType                       10i 0 Options(*Omit)
     D error                      32767a   Options(*VarSize: *Omit) NoOpt


      //
      // Prototype for QLICVTTP API.
      //
      // "Convert an object type from the external symbolic format to the internal hexadecimal
      // format and vice versa."
      //
      // http://publib.boulder.ibm.com/iseries/v5r2/ic2924/info/apis/qlicvttp.htm
      //

     DQLICVTTP         PR                  ExtPgm('QLICVTTP')
     D cvtType                       10a   Const
     D objType                       10a   Const
     D hexType                        2a
     D error                      32767a   Options(*VARSIZE:*OMIT) NoOpt


       //----------------------------------------------------------------------
       //   Symbolic Constants
       //----------------------------------------------------------------------

       // Call stack entry:
       // - current call stack entry
     D CUR_CALL_STK_ENT...
     D                 c                   Const('*')
       // - control boundary
     D CONTROL_BOUNDARY...
     D                 c                   Const('*CTLBDY')


      /endif
      