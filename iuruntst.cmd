             CMD        PROMPT('ILEUnit v0.1.0 - Run Tests')

             PARM       KWD(TSTPGM) TYPE(OBJ) MIN(1) PROMPT('Test program')
             PARM       KWD(TSTPRC) TYPE(*CHAR) LEN(32) DFT(*ALL) SPCVAL(*ALL) PROMPT('Test +
                          procedure')
             PARM       KWD(ORDER) TYPE(*CHAR) LEN(8) RSTD(*YES) DFT(*API) VALUES(*API +
                          *REVERSE) PROMPT('Run order')
             PARM       KWD(DETAIL) TYPE(*CHAR) LEN(6) RSTD(*YES) +
                          DFT(*BASIC) VALUES(*BASIC *ALL) +
                          PROMPT('Report detail')

 OBJ:        QUAL       TYPE(*NAME) LEN(10)
             QUAL       TYPE(*NAME) LEN(10) DFT(*LIBL) SPCVAL(*LIBL) PROMPT('Library')

