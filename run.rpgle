      //
      // Run tests.
      //

     H nomain

      //----------------------------------------------------------------------
      //   Prototypes
      //----------------------------------------------------------------------
      /include 'run_h.rpgle'
      /include 'assert_h.rpgle'
      /include 'callproc_h.rpgle'
      /include 'extprc_h.rpgle'
      /include 'exttst_h.rpgle'
      /include 'message_h.rpgle'
      /include 'llist/llist_h.rpgle'


      *-------------------------------------------------------------------------
      * Templates
      *-------------------------------------------------------------------------
      /include 'ileunit_t.rpgle'


      //----------------------------------------------------------------------
      //   Procedure Definitions
      //----------------------------------------------------------------------

       //----------------------------------------------------------------------
       // Load a test suite. See prototype.
       //----------------------------------------------------------------------
     P loadTestSuite   b                   Export
     D loadTestSuite   pi                  LikeDs(TestSuite_t)
     D  srvPgm                             Const LikeDs(Object_t)

     D procList        ds                  LikeDs(ProcList_t)
     D procNmList      ds                  LikeDs(ProcNmList_t)
     D testSuite       ds                  LikeDs(TestSuite_t)
     D actMark         s                   Like(ActMark_t)

      /free

        procList   = loadProcList( srvPgm );
        procNmList = getProcNmList( procList );
        testSuite  = getTestSuite( procNmList );

        actMark = activateSrvPgm( srvPgm );
        activateTestSuite( testSuite : actMark );

        return testSuite;

      /end-free
     P                 e


       //----------------------------------------------------------------------
       // Reclaim a test suite's allocated ressources. See prototype.
       //----------------------------------------------------------------------
     P rclTestSuite    b                   Export
     D rclTestSuite    pi
     D  testSuite                          LikeDs(TestSuite_t)
      /free

        dealloc(n) testSuite.testList;
        list_dispose(testSuite.testResults);

      /end-free
     P                 e


       //----------------------------------------------------------------------
       // Run a test case in a test suite. See prototype.
       //----------------------------------------------------------------------
     P runTest         b                   Export
     D runTest         pi                  LikeDs(TestResult_t)
     D  testSuite                          Const LikeDs(TestSuite_t)
     D  testIdx                      10i 0 Const

     D testCase        ds                  LikeDs(Proc_t)

      /free

        testCase = getTestProc( testSuite : testIdx );

        return runTestProc( testCase.procPtr :
                            testSuite.setUp.procPtr :
                            testSuite.tearDown.procPtr );

      /end-free
     P                 e


       //----------------------------------------------------------------------
       // Run a test case. See prototype.
       //----------------------------------------------------------------------
     P runTestProc     b                   Export
     D runTestProc     pi                  LikeDs(TestResult_t)
     D  testProc                       *   Const ProcPtr
     D  setUp                          *   Const ProcPtr
     D  tearDown                       *   Const ProcPtr

     D failureEvt      ds                  LikeDs(assert_failevent_t)
     D result          ds                  LikeDs(TestResult_t)
      /free
        clear result;

        assert_clearFailEvent();
        result.outcome = TEST_CASE_SUCCESS;

        if setUp <> *null;

          monitor;
            callProcByPtr( setUp );
          on-error;
            result.outcome = TEST_CASE_ERROR;
            result.error = rcvExcpMsgInfo();
          endmon;

        endif;

        if result.outcome = TEST_CASE_SUCCESS;

          monitor;
            callProcByPtr( testProc );
          on-error;
            failureEvt = assert_getFailEvent();
            if failureEvt = *blank;
              result.outcome = TEST_CASE_ERROR;
              result.error = rcvExcpMsgInfo();
              result.errorCount = 1;
            else;
              result.outcome = TEST_CASE_FAILURE;
              result.failure = failureEvt;
              result.failureCount = 1;
            endif;
          endmon;

        endif;

        if tearDown <> *null;

          monitor;
            callProcByPtr( tearDown );
          on-error;
            result.outcome = TEST_CASE_ERROR;
            result.error = rcvExcpMsgInfo();
          endmon;

        endif;

        return result;

      /end-free
     P                 e