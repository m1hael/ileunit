     /**
      * \brief ILEUnit : Output Interface
      *
      *
      * \author Mihael Schmidt
      * \date   08.02.2011
      */


     H nomain


      *-------------------------------------------------------------------------
      * Prototypen
      *-------------------------------------------------------------------------
     D writer_check    PR
     D   writer                        *   const
 
      /include 'ileuout_h.rpgle'
      /include 'message_h.rpgle'


      *-------------------------------------------------------------------------
      * Templates
      *-------------------------------------------------------------------------
      /include 'ileuout_t.rpgle'


     P ileunit_writer_outputTestResults...
     P                 B                   export
     D                 PI
     D   writer                        *   const
     D   testSuite                   50A   const
     D   testResults                   *   const
      *
     D procedurePointer...
     D                 S               *   procptr
     D output          PR                  extproc(procedurePointer)
     D   writer                        *   const
     D   testSuite                   50A   const
     D   testResults                   *   const
      *
     D header          DS                  likeds(ileunit_writer_t)
     D                                     based(writer)
      /free
       procedurePointer = header.proc_output;

       writer_check(writer);

       output(writer : testSuite : testResults);
      /end-free
     P                 E


     P ileunit_writer_finalize...
     P                 B                   export
     D                 PI
     D   writer                        *
      *
     D header          DS                  likeds(ileunit_writer_t)
     D                                     based(writer)
     D procedurePointer...
     D                 S               *   procptr
     D finalize        PR                  extproc(procedurePointer)
     D   writer                        *
      /free
       procedurePointer = header.proc_finalize;

       writer_check(writer);

       finalize(writer);

       if (writer <> *null);
         dealloc(n) writer;
       endif;
      /end-free
     P                 E


     /**
      * \brief Check writer
      *
      * Checks if the writer provides procedure pointers for all
      * procedures it should implement.
      *
      * \param Writer
      *
      * \throws CPF9898 Incomplete implementation
      */
     P writer_check...
     P                 B
     D                 PI
     D   writer                        *   const
      *
     D header          DS                  likeds(ileunit_writer_t)
     D                                     based(writer)
     D cachedWriterId  S                   like(ileunit_writer_t.id)
     D                                     static
     D writerChecked   S               N   inz(*off) static
      /free
       if (header.id = cachedWriterId and
           writerChecked);
         return;
       endif;

       cachedWriterId = header.id;

       if (header.proc_finalize = *null);
         sndEscapeMsg('ILEUnit Writer Interface: ' +
             'Incomplete Implementation ' + %trimr(header.id) +  '.' :
             TWO_CALL_STK_LVL_ABOVE);
       elseif (header.proc_output = *null);
         sndEscapeMsg('ILEUnit Writer Interface: ' +
             'Incomplete Implementation ' + %trimr(header.id) +  '.' :
             TWO_CALL_STK_LVL_ABOVE);
       endif;

       writerChecked = *on;
      /end-free
     P                 E
