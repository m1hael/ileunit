     /**
      * \brief Run test suite for remote interfaces
      *
      *
      * \author Mihael Schmidt
      * \date   16.02.2010
      */

     H nomain


      *-------------------------------------------------------------------------
      * Prototypen
      *-------------------------------------------------------------------------
     D isTestRunSuccessful...
     D                 PR              N
     D   testResults                   *   const

      /include 'runrmt_h.rpgle'
      /include 'assert_h.rpgle'
      /include 'callproc_h.rpgle'
      /include 'cmdrunlog_h.rpgle'
      /include 'message_h.rpgle'
      /include 'cmdrun_h.rpgle'
      /include 'llist/llist_h.rpgle'
      /include 'ileuout_h.rpgle'
      /include 'error_h.rpgle'
      /include 'run_h.rpgle'


      *-------------------------------------------------------------------------
      * Datenstrukturen
      *-------------------------------------------------------------------------
     D tmpl_testSuite  DS                  qualified based(nullPointer)
     D  testSuite                          likeds(object_t)
     D  numberTestCases...
     D                               10I 0
     D  numberAssertions...
     D                               10I 0
     D  numberFailures...
     D                               10I 0
     D  numberErrors...
     D                               10I 0
     D  offsetTestCases...
     D                               10I 0
     D  reserved1                   100A
      *
     D tmpl_testCase   DS                  qualified based(nullPointer)
     D  testCase                    100A
     D  result                        1A
     D  numberAssertions...
     D                               10I 0
      *
      /include 'ileunit_t.rpgle'


      *-------------------------------------------------------------------------
      * Prozeduren
      *-------------------------------------------------------------------------


     P isTestRunSuccessful...
     P                 B
     D                 PI              N
     D   testResults                   *   const
      *
     D successful      S               N   inz(*on)
     D testResult      DS                  likeds(TestResult_t) based(ptr)
      /free
       list_abortIteration(testResults);

       ptr = list_getNext(testResults);
       dow (ptr <> *null);

         if (testResult.failureCount > 0 or
             testResult.errorCount > 0);
           successful = *off;
           list_abortIteration(testResults);
           leave;
         endif;

         ptr = list_getNext(testResults);
       enddo;

       return successful;
      /end-free
     P                 E


     P ileunit_runTestSuite...
     P                 B                   export
     D                 PI            10I 0
     D   writer                        *   const
     D   testSuiteName...
     D                               10A   const
     D   pTestSuiteLibrary...
     D                               10A   const options(*nopass)
      *
     D testSuiteLibrary...
     D                 S             10A   inz('*LIBL')
     D testSuite       DS                  likeds(testSuite_t)
     D returnValue     S             10I 0
      *
     D testProc        S             32A   inz('*ALL')
     D order           S              8A   inz('*API')
     D detail          S              6A   inz('*BASIC')
      /free
       if (%parms() = 3);
         testSuiteLibrary = pTestSuiteLibrary;
       endif;

       setLogContext( testSuiteName + testSuiteLibrary);

       assert_clearCounter();

        monitor;
          testSuite = loadTestSuite( testSuiteName + testSuiteLibrary );
        on-error;
          returnValue = -1;
          raiseRUError( 'Error while loading the test suite in '
                      + fmtObjNm(testSuiteName + testSuiteLibrary) + '.' );
        endmon;

        if (testSuite.testCasesCnt = 0);
          returnValue = -2;
          raiseRUError( 'No test case found in service program '
                      + fmtObjNm(testSuiteName + testSuiteLibrary) + '.' );
        endif;

        callWithLogging( testSuite.setUpSuite );

        runTests( testSuiteName : testSuite : testProc : order : detail );

        ileunit_writer_outputTestResults(
            writer : testSuiteName + testSuiteLibrary : testSuite.testResults);

        if (not isTestRunSuccessful(testSuite.testResults));
          returnValue = -4;
        endif;

        callWithLogging( testSuite.tearDownSuite );

        monitor;
          rclTestSuite( testSuite );
        on-error;
          returnValue = -3;
          raiseRUError( 'Failed to reclaim the test suite''s resources.' );
        endmon;

       return returnValue;
      /end-free
     P                 E

