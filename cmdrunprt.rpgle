     /**
      * \brief ILEUnit : Command Line Runner Printing Facilities
      *
      */
      
     H nomain

      //----------------------------------------------------------------------
      //   Files
      //----------------------------------------------------------------------

     FQSYSPRT   o    f   80        printer OFLIND(*INOF) UsrOpn


      //----------------------------------------------------------------------
      //   Prototypes
      //----------------------------------------------------------------------
      /include 'cmdrunprt_h.rpgle'
      /include 'libc_h.rpgle'


      //----------------------------------------------------------------------
      //   Templates
      //----------------------------------------------------------------------
      /include 'ileunit_t.rpgle'
      

      //----------------------------------------------------------------------
      //   Global Variables
      //----------------------------------------------------------------------

       // Line for printing in log.
     D logLine         s             80a

     OQSYSPRT   e            ExcpLine    1
     O                       logLine             80


      //----------------------------------------------------------------------
      //   Procedures
      //----------------------------------------------------------------------

     P clsPrt          b                   Export
       //----------------------------------------------------------------------
       // Close the printer file. See prototype.
       //----------------------------------------------------------------------
     D clsPrt          pi
      /free

        close QSYSPRT;
        system( 'DLTOVR FILE(QSYSPRT)' );

      /end-free
     P                 e


     P getPrtWidth     b                   Export
     D getPrtWidth     pi            10i 0
      /free

        return %len( logLine );

      /end-free
     P                 e


     P opnPrt          b                   Export
     D opnPrt          pi
     D  testPgmNm                          Const Like(Object_t.nm)
      /free

        system( 'OVRPRTF FILE(QSYSPRT) '
             + 'USRDTA(' + testPgmNm + ') '
             + 'SPLFNAME(RPGUNIT)' );
        open QSYSPRT;

      /end-free
     P opnPrt          e


     P prtLine         b                   Export
       //----------------------------------------------------------------------
       // Print a line. See prototype.
       //----------------------------------------------------------------------
     D prtLine         pi
     D  line                         80a   Const
      /free

        logLine = line;
        except ExcpLine;

      /end-free
     P                 e


