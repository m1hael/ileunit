      //
      // Utilities.
      //

     H nomain


       //----------------------------------------------------------------------
       //   Public Procedure Prototypes
       //----------------------------------------------------------------------
      /include 'utilities_h.rpgle'


       //----------------------------------------------------------------------
       //   Imported Procedures
       //----------------------------------------------------------------------
      /include 'userspace_h.rpgle'


       //----------------------------------------------------------------------
       // Templates
       //----------------------------------------------------------------------
      /include 'ileunit_t.rpgle'
      /if not defined(QUSEC)
      /define QUSEC
      /include QSYSINC/QRPGLESRC,QUSEC
      /endif


       //----------------------------------------------------------------------
       //   Procedures Definition
       //----------------------------------------------------------------------

     /**
      * \brief Creates a user space
      * 
      * Returns a pointer to a created userspace. The qualified name is
      * returned via the first parameter.
      * 
      * \param Userspace name and library
      * \param Userspace object text
      * 
      * \return Pointer to created userspace
      */
     P ileunit_createUserspace...
     P                 B                   export
     D                 PI              *
     D  userspace                          const likeds(object_t)
     D  text                         50A   const
      *
     D usrSpc_p        s               *
      /free
        clear QUSEC;
        QUSBPRV = 0;
        
        userspace_create(userspace :
                         'USRSPC' :
                         1024 :
                         x'00' :
                         '*ALL' :
                         text :
                         '*YES' :
                         QUSEC );

        userspace_retrievePointer(userspace : usrSpc_p );

        return usrSpc_p;
      /end-free
     P                 E
