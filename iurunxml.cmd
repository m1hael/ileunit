             CMD        PROMPT('ILEUnit v0.1.0 - XML output')

             PARM       KWD(IFS) TYPE(*CHAR) MIN(1) LEN(100) PROMPT('XML output file')
             PARM       KWD(TSTLIB) TYPE(*NAME) MIN(1) LEN(10) PROMPT('Test library')
             PARM       KWD(TSTPGM) TYPE(*NAME) MIN(1) LEN(10) PROMPT('Test program')
