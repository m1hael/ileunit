
void assert_equalsInteger(const int *expected, const int *actual);

void assert_fail(const char * s);

void assert_assertEquals(const char * expected, const char * actual);

void assert_assertTrue(const char condition , const char * message);
