      /if not defined (ILEUNIT_TEMPLATES)
      /define ILEUNIT_TEMPLATES

      //
      // RPGUnit Data Templates.
      //

       // ILE Activation Mark.
     D ActMark_t       s             10I 0 template

     D assert_failevent_t...
     D                 DS                  qualified template
     D  msg                         256A
     D  callStkEnt                         likeds(callstackentry_t)
     D                                     dim(MAX_CALL_STK_SIZE)
      
     D callstackentry_t...
     D                 DS                  qualified template
     D  pgmNm                        10A
     D  modNm                        10A
     D  procNm                      256A
     D  specNb                       10A
     D 
       // TODO Deprecate this template in favor of Msg_t.
       // Exception Message Information.
     D ExcpMsgInfo_t   ds                  Qualified Based(template)
     D  msgId                         7a
     D  msg                         256a
        // Sending Program Name.
     D  pgmNm                        12a
        // Sending Procedure Name.
     D  procNm                             Like(ProcNm_t)
        // Sending Statement Number.
     D  sttNb                        10a

       // Program Message.
     D Msg_t           ds                  Qualified Based(template)
     D  id                            7a
     D  txt                         254a   Varying
     D  rplData                     254a   Varying
     D  sender                             LikeDs(MsgSender_t)

       // Program message sender.
     D MsgSender_t     ds                  Qualified Based(template)
     D  pgmNm                        12a
     D  procNm                             Like(ProcNm_t)
     D  sttNb                         6a

       // Object Qualified Name.
     D Object_t        ds                  Qualified Based(template)
     D  nm                           10a
     D  lib                          10a

       // Named callable procedure.
     D Proc_t          ds                  Qualified Based(template)
     D  procNm                             Like(ProcNm_t)
     D  procPtr                        *   ProcPtr

     D ProcNmList_t    ds                  Qualified Based(template)
     D  handle                         *
     D  cnt                            *   ProcPtr
     D  getNm                          *   ProcPtr
     D  goToNext                       *   ProcPtr

     D ProcNm_t        s            256a   Based(template)

       // Qualified Job Name.
     D QlfJobNm_t      ds                  Qualified Based(template)
     D  jobNm                        10a
     D  usrNm                        10a
     D  jobNb                         6a

     D testresult_t    DS                  qualified template
     D  outcome                       1A
     D  details
     D  failure                            likeds(assert_failevent_t)
     D                                     Overlay(details)
     D  error                              LikeDs(ExcpMsgInfo_t)
     D                                     Overlay(details)
     D  testName                    256A
     D  assertCount                  10i 0
     D  errorCount                   10i 0
     D  failureCount                 10i 0

     D testsuite_t     DS                  qualified template
     D  setUpSuite                         LikeDs(Proc_t)
     D  setUp                              LikeDs(Proc_t)
     D  testCasesCnt                 10i 0
     D  testList                       *
     D  tearDown                           LikeDs(Proc_t)
     D  teardownSuite                      LikeDs(Proc_t)
     D  testResults                    *

       // User profile name.
     D UsrNm_t         S             10a   Based(template)

       // TestResult_t.outcome can have three values.
     D TEST_CASE_SUCCESS...
     D                 C                   'S'
     D TEST_CASE_FAILURE...
     D                 C                   'F'
     D TEST_CASE_ERROR...
     D                 C                   'E'

     D MAX_CALL_STK_SIZE...
     D                 C                   64
     D 
      /endif
      