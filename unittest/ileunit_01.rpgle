     /**
     * \brief ILEUnit : Assert test
   *
   * \author Mihael Schmidt
   * \date   2012-07-29
   */

     H nomain

      *-------------------------------------------------------------------------
      * Prototypes
      *-------------------------------------------------------------------------
     D test_assert...
     D                 PR

      /copy 'ileunit/assert_h.rpgle'

     P test_assert...
     P                 B                   export
      *
     D list            S               *
   /free
       assert_assertTrue(list = *null:'Not created list object must be null.');
       assert_equalsInteger(1 : 1);
       assert_assertEquals('Mihael' : 'Mihael');
   /end-free
     P                 E
