       //----------------------------------------------------------------------
       // Program for creating Test Suites
       //----------------------------------------------------------------------

       //----------------------------------------------------------------------
       //   Templates
       //----------------------------------------------------------------------
       // A sized array of OS400 objects.
     D ObjectArray_t   ds                  Based(template) Qualified
     D  size                          5i 0
     D  object                             Dim(3) LikeDs(Object_t)

       // A sized list of options.
     D Options_t       ds                  Based(template) Qualified
     D  size                          5i 0
     D  option                       10a   Dim(3)

      /include 'ileunit_t.rpgle'
      

       //----------------------------------------------------------------------
       //   Prototypes
       //----------------------------------------------------------------------
      /include 'message_h.rpgle'
      /include 'libc_h.rpgle'


     D QCMDEXC         PR                  extpgm('QCMDEXC')
     D  cmd                        1024A   const options(*varsize)
     D  cmdLen                       15P 5 const


       //----------------------------------------------------------------------
       //   PRIVATE PROTOTYPES
       //----------------------------------------------------------------------

     D crtRpgMod       pr
     D  testPgm                            Const LikeDs(Object_t)
     D  srcFile                            Const LikeDs(Object_t)
     D  srcstmf                     100A   const
     D  cOption                            Const LikeDs(Options_t)
     D  dbgView                      10a   Const
     D  incdir                      100A   const
     D  buildLib                     10A   const

     D crtSrvPgm       pr                  LikeDs(Object_t)
     D  testPgm                            Const LikeDs(Object_t)
     D  bndSrvPgm                          Const LikeDs(ObjectArray_t)
     D  bndDir                             Const LikeDs(ObjectArray_t)
     D  bOption                            Const LikeDs(Options_t)
     D  buildLib                     10A   const

     D tagUnitTest     PR                  extpgm('IUUPDOBJD')
     D   library                     10A   const
     D   object                      10A   const

     D getCrtRpgModCmd...
     D                 pr           256a
     D  pgm                                Const LikeDs(Object_t)
     D  srcFile                            Const LikeDs(Object_t)
     D  srcstmf                     100A   const
     D  options                            Const LikeDs(Options_t)
     D  dbgView                      10a   Const
     D  incdir                      100A   const
     D  buildLib                     10A   const

     D getCrtSrvPgmCmd...
     D                 pr           256a
     D  pgm                                Const LikeDs(Object_t)
     D  bndSrvPgm                          Const LikeDs(ObjectArray_t)
     D  bndDir                             Const LikeDs(ObjectArray_t)
     D  options                            Const LikeDs(Options_t)
     D  export                       10a   Const
     D  buildLib                     10A   const

     D serializeObjectArray...
     D                 pr           256a   Varying
     D  headToken                    10a   Const
     D  array                              Const LikeDs(ObjectArray_t)

     D serializeObjectName...
     D                 pr            21a   Varying
     D  object                             Const LikeDs(Object_t)

     DserializeOptions...
     D                 pr           256a   Varying
     D headToken                     10a   Const
     D options                             Const LikeDs(Options_t)
     
     
       //----------------------------------------------------------------------
       //   GLOBAL VARIABLES
       //----------------------------------------------------------------------

     D srvPgm          ds                  LikeDs(Object_t)


       //----------------------------------------------------------------------
       //   MAIN PROGRAM
       //----------------------------------------------------------------------

     Diucrttst         pr                  extpgm('IUCRTTST')
     D testPgm                             Const LikeDs(Object_t)
     D srcFile                             Const LikeDs(Object_t)
     D srcstmf                      100A   const
     D cOption                             Const LikeDs(Options_t)
     D dbgView                       10a   Const
     D bndSrvPgm                           Const LikeDs(ObjectArray_t)
     D bndDir                              Const LikeDs(ObjectArray_t)
     D bOption                             Const LikeDs(Options_t)
     D incdir                       100A   const
     D buildLib                      10A   const

     Diucrttst         pi
     D testPgm                             Const LikeDs(Object_t)
     D srcFile                             Const LikeDs(Object_t)
     D srcstmf                      100A   const
     D cOption                             Const LikeDs(Options_t)
     D dbgView                       10a   Const
     D bndSrvPgm                           Const LikeDs(ObjectArray_t)
     D bndDir                              Const LikeDs(ObjectArray_t)
     D bOption                             Const LikeDs(Options_t)
     D incdir                       100A   const
     D buildLib                      10A   const
     
      /free

       monitor;
         crtRpgMod( testPgm : srcFile : srcstmf : cOption : dbgView : incdir :
                    buildLib );
         srvPgm = crtSrvPgm( testPgm : bndSrvPgm : bndDir : bOption : buildLib);

         tagUnitTest( srvPgm.lib : srvPgm.nm );

         sndCompMsg( 'Test program ' + %trim(srvPgm.nm)
                   + ' created in library ' + %trim(srvPgm.lib) + '.' );

       on-error;
         sndEscapeMsgAboveCtlBdy( 'Unable to create test '
                                  + %trim(testPgm.nm) + '.' );
       endmon;

       *inlr = *on;

      /end-free


     P crtRpgMod       b
     D crtRpgMod       pi
     D  testPgm                            Const LikeDs(Object_t)
     D  srcFile                            Const LikeDs(Object_t)
     D  srcstmf                     100A   const
     D  cOption                            Const LikeDs(Options_t)
     D  dbgView                      10a   Const
     D  incdir                      100A   const
     D  buildLib                     10A   const

       // A command to be executed.
     D cmd             s            256a   Varying
      /free

         cmd = getCrtRpgModCmd( testPgm : srcFile : srcstmf : cOption : 
                                dbgView : incdir : buildLib );
         sndInfoMsg( cmd );
         system( cmd );

      /end-free
     P                 e


     P crtSrvPgm       b
     D crtSrvPgm       pi                  LikeDs(Object_t)
     D  testPgm                            Const LikeDs(Object_t)
     D  bndSrvPgm                          Const LikeDs(ObjectArray_t)
     D  bndDir                             Const LikeDs(ObjectArray_t)
     D  bOption                            Const LikeDs(Options_t)
     D  buildLib                     10A   const

       // Export all procedures from the service program.
     D EXPORT_ALL      c                   Const('*ALL')
       // A command to be executed.
     D cmd             s            256a   Varying
       // Bound Service Programs, with RPGUnit services included.
     D bndSrvPgmWithIU...
     D                 ds                  LikeDs(ObjectArray_t)
       // Completion message data.
     D compMsgData     s            256a
       // Target service program.
     D targetSrvPgm    ds                  LikeDs(Object_t)

      /free

         bndSrvPgmWithIU = bndSrvPgm;
         bndSrvPgmWithIU.size += 1;
         bndSrvPgmWithIU.object(bndSrvPgmWithIU.size).nm = 'ASSERT';
         bndSrvPgmWithIU.object(bndSrvPgmWithIU.size).lib = '*LIBL';

         cmd = getCrtSrvPgmCmd( testPgm :
                                bndSrvPgmWithIU :
                                bndDir :
                                bOption :
                                EXPORT_ALL : 
                                buildLib);
         sndInfoMsg( cmd );
         qcmdexc( cmd : %len(cmd) );

         compMsgData = rcvMsgData( '*COMP' );
         targetSrvPgm = %subst( compMsgData : 1 : 20 );

         return targetSrvPgm;

      /end-free
     P                 e


     P getCrtSrvPgmCmd...
     P                 b                   Export
     D getCrtSrvPgmCmd...
     D                 pi           256a
     D  testPgm                            Const LikeDs(Object_t)
     D  bndSrvPgm                          Const LikeDs(ObjectArray_t)
     D  bndDir                             Const LikeDs(ObjectArray_t)
     D  options                            Const LikeDs(Options_t)
     D  export                       10a   Const
     D  buildLib                     10A   const

       // Command string accumulator.
     D cmd             s            256a   Varying
       // Counter.
     D i               s             10i 0

      /free

        cmd = 'CRTSRVPGM SRVPGM(' + serializeObjectName( testPgm ) + ') ';
        cmd += 'MODULE(QTEMP/' + %trim(testPgm.nm) + ') ';
        cmd += serializeObjectArray( 'BNDSRVPGM' : bndSrvPgm );
        cmd += serializeObjectArray( 'BNDDIR'    : bndDir    );
        cmd += serializeOptions( 'OPTION' : options );

        if export <> *blank;
          cmd += 'EXPORT(' + %trim(export) + ') ';
        endif;

        return cmd;

      /end-free
     P getCrtSrvPgmCmd...
     P                 e


     P getCrtRpgModCmd...
     P                 b                   Export
     D getCrtRpgModCmd...
     D                 pi           256a
     D  testPgm                            Const LikeDs(Object_t)
     D  srcFile                            Const LikeDs(Object_t)
     D  srcstmf                     100A   const
     D  options                            Const LikeDs(Options_t)
     D  dbgView                      10a   Const
     D  incdir                      100A   const
     D  buildLib                     10A   const

       // Command string accumulator.
     D cmd             s            256a   Varying
       // Counter.
     D i               s             10i 0

      /free

        cmd = 'CRTRPGMOD MODULE(QTEMP/' + %trim(testPgm.nm) + ') ';

        if (srcstmf <> *blank);
          cmd += ' SRCSTMF(''' + %trim(srcstmf) + ''') ';
        else;
        
          if srcFile <> *blank;
            cmd += 'SRCFILE(' + serializeObjectName(srcFile) + ') ';
          endif;
          
        endif;

        cmd += serializeOptions( 'OPTION' : options );

        if dbgView <> *blank;
          cmd += 'DBGVIEW(' + %trim(dbgView) + ') ';
        endif;

        if (incdir <> *blank);
          cmd += ' INCDIR(''' + %trim(incdir) + ''')';
        endif;

        return cmd;

      /end-free
     P getCrtRpgModCmd...
     P                 e


     PserializeObjectArray...
     P                 b                   Export
     DserializeObjectArray...
     D                 pi           256a   Varying
     D headToken                     10a   Const
     D array                               Const LikeDs(ObjectArray_t)

       // Serialized Object Array.
     D serialized      s            256a   Varying
       // Counter.
     D i               s             10i 0

      /free

        if array.size = 0;
          return '';
        endif;

        serialized = %trim(headToken) + '(';

        for i = 1 to array.size;
          serialized += serializeObjectName( array.object(i) ) + ' ';
        endfor;

        serialized += ') ';

        return serialized;

      /end-free
     PserializeObjectArray...
     P                 e


     P serializeObjectName...
     P                 b                   Export
     D serializeObjectName...
     D                 pi            21a   Varying
     D  object                             Const LikeDs(Object_t)

       // Serialized object name.
     D serialized      s            256a   Varying Inz('')

      /free

        if object.lib <> *blank;
          serialized += %trim(object.lib) + '/';
        endif;

        serialized += %trim(object.nm);

        return serialized;

      /end-free
     P serializeObjectName...
     P                 e


     PserializeOptions...
     P                 b                   Export
     DserializeOptions...
     D                 pi           256a   Varying
     D headToken                     10a   Const
     D options                             Const LikeDs(Options_t)

       // Serialized Options.
     D serialized      s            256a   Varying
       // Counter.
     D i               s             10i 0

      /free

        if options.size = 0;
          return '';
        endif;

        serialized = 'OPTION(';

        for i = 1 to options.size;
          serialized += %trim(options.option(i)) + ' ';
        endfor;

        if options.size > 0;
          serialized += ') ';
        endif;

        return serialized;

      /end-free
     PserializeOptions...
     P                 e
