     /**
     * \brief ILEUnit : Run test suite with output to a stream file
   *
   * \author Mihael Schmidt
   * \date   02.09.2012
      * 
      * \param Stream file path
      * \param Testsuite Library
      * \param Testsuite Name
   */

     D IU_RUN_XML      PR                  extpgm('IU_RUN_XML')
     D   file                       100A
     D   testlib                     10A
     D   testsuite                   10A
     D IU_RUN_XML      PI
     D   file                       100A
     D   testlib                     10A
     D   testsuite                   10A
     
      *-------------------------------------------------------------------------
      * Prototypen
      *-------------------------------------------------------------------------
      /include 'ileuoxml_h.rpgle'
      /include 'runrmt_h.rpgle'
       
     D main            PR 
     D   file                       100A   const
     D   testlib                     10A   const
     D   testsuite                   10A   const
       
      /free
       main(%trimr(file) + x'00' : testlib : testsuite);
       
       *inlr = *on;
      /end-free
      
     
     P main            B
     D                 PI
     D   file                       100A   const
     D   testlib                     10A   const
     D   testsuite                   10A   const
      *
     D writer          S               *
      /free
       writer = ileunit_writer_xml_create(file);
       
       ileunit_runTestSuite(writer : testsuite : testlib);
       
       ileunit_writer_xml_finalize(writer);
      /end-free
     P                 E
     