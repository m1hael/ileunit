     /**
      * \brief ILEUnit Writer : Userspace
      *
      *
      * \author Mihael Schmidt
      * \date   09.02.2011
      */


     H nomain


      *-------------------------------------------------------------------------
      * Prototypen
      *-------------------------------------------------------------------------
     D calculateResultSums...
     D                 PR                  likeds(sum_result_t)
     D   testResults                   *   const
      *
      /include 'ileuous_h.rpgle'
      /include 'llist/llist_h.rpgle'
      /include 'userspace_h.rpgle'


      *-------------------------------------------------------------------------
      * Templates
      *-------------------------------------------------------------------------
     D userdata_t      DS                  qualified template align
     D   userspaceName...
     D                               10A
     D   userspaceLibrary...
     D                               10A
     D   userspacePointer...
     D                                 *
      *
     D sum_result_t    DS                  qualified template
     D   assert                      10I 0
     D   failure                     10I 0
     D   error                       10I 0
     D   test                        10I 0

      /include 'ileuout_t.rpgle'
      /include 'ileunit_t.rpgle'


      *-------------------------------------------------------------------------
      * Constants
      *-------------------------------------------------------------------------
     D ILEUNIT_WRITER_USERSPACE_ID...
     D                 C                   'ileunit_writer_userspace'


      *-------------------------------------------------------------------------
      * Procedures
      *-------------------------------------------------------------------------


     P ileunit_writer_userspace_create...
     P                 B                   export
     D                 PI              *
     D   userspaceLibrary...
     D                               10A   const
     D   userspaceName...
     D                               10A   const
      *
     D header          DS                  likeds(ileunit_writer_t)
     D                                     based(writer)
     D userdata        DS                  likeds(userdata_t)
     D                                     based(header.userdata)
      /free
       writer = %alloc(%size(ileunit_writer_t));

       header.id = ILEUNIT_WRITER_USERSPACE_ID;
       header.userdata = %alloc(%size(userdata_t));
       header.proc_output = %paddr('ileunit_writer_userspace_output');
       header.proc_finalize = %paddr('ileunit_writer_userspace_finalize');
       userdata.userspaceName = userspaceName;
       userdata.userspaceLibrary = userspaceLibrary;
       clear QUSEC;
       userspace_retrievePointer(userspaceName + userspaceLibrary :
                                 userdata.userspacePointer : QUSEC);
       return writer;
      /end-free
     P                 E


     P ileunit_writer_userspace_output...
     P                 B                   export
     D                 PI
     D   writer                        *   const
     D   testSuite                   50A   const
     D   testResults                   *   const
      *
     D header          DS                  likeds(ileunit_writer_t)
     D                                     based(writer)
     D udata           DS                  likeds(userdata_t)
     D                                     based(header.userdata)
      *
     D usHeader_t      DS                  qualified based(nullPointer)
     D  testSuite                          likeds(object_t)
     D  numberTestCases...
     D                               10I 0
     D  numberAssertions...
     D                               10I 0
     D  numberFailures...
     D                               10I 0
     D  numberErrors...
     D                               10I 0
     D  offsetTestCases...
     D                               10I 0
     D  reserved1                   100A
      *
     D entry_t         DS                  qualified based(nullPointer)
     D  testCase                    100A
     D  result                        1A
     D  numberAssertions...
     D                               10I 0
      *
     D i               S             10I 0
     D usHeader        DS                  likeds(usHeader_t)
     D                                     based(udata.userspacePointer)
     D entry           DS                  likeds(entry_t) based(eptr)
     D testResult      DS                  likeds(TestResult_t) based(ptr)
     D sumResult       DS                  likeds(sum_result_t)
      /free
       sumResult = calculateResultSums(testResults);

       clear usHeader;
       usHeader.testSuite = testSuite;
       usHeader.numberTestCases = sumResult.test;
       usHeader.numberAssertions = sumResult.assert;
       usHeader.numberFailures = sumResult.failure;
       usHeader.numberErrors = sumResult.error;
       usHeader.reserved1 = *blank;
       usHeader.offsetTestCases = %size(usHeader);

       list_abortIteration(testResults);
       ptr = list_getNext(testResults);
       dow (ptr <> *null);
         eptr = udata.userspacePointer + %size(usHeader) + (i * %size(entry));
         entry.testCase = testResult.testName;
         entry.result = testResult.outcome;
         entry.numberAssertions = testResult.assertCount;

         ptr = list_getNext(testResults);
         i += 1;
       enddo;
      /end-free
     P                 E


     P ileunit_writer_userspace_finalize...
     P                 B                   export
     D                 PI
     D   writer                        *
      *
     D header          DS                  likeds(ileunit_writer_t)
     D                                     based(writer)
     D userdata        DS                  likeds(userdata_t)
     D                                     based(header.userdata)
      /free
       if (writer <> *null);

         if (header.userdata <> *null);
           dealloc header.userdata;
         endif;

         dealloc(n) writer;
       endif;
      /end-free
     P                 E


     P calculateResultSums...
     P                 B
     D                 PI                  likeds(sum_result_t)
     D   testResults                   *   const
      *
     D result          DS                  likeds(sum_result_t)
     D testResult      DS                  likeds(TestResult_t) based(ptr)
      /free
       clear result;

       list_abortIteration(testResults);

       ptr = list_getNext(testResults);
       dow (ptr <> *null);

         result.assert += testResult.assertCount;
         result.failure += testResult.failureCount;
         result.error += testResult.errorCount;
         result.test += 1;

         ptr = list_getNext(testResults);
       enddo;

       return result;
      /end-free
     P                 E
