     /**
      * \brief ILEUnit : Assert Module
      *
      *
      */
      
     H nomain

       //----------------------------------------------------------------------
       //   Prototypes
       //----------------------------------------------------------------------
     D getCallStack    PR                  likeds(callstackentry_t)
     D                                     dim(MAX_CALL_STK_SIZE)
    
      /include 'assert_h.rpgle'
      /include 'message_h.rpgle'
      /include 'system_h.rpgle'


       //----------------------------------------------------------------------
       //   Templates
       //----------------------------------------------------------------------
      /include 'ileunit_t.rpgle'


       //----------------------------------------------------------------------
       //   CONSTANTS
       //----------------------------------------------------------------------
      // Quote (') symbol.
     D quote           c                   Const('''')


       //----------------------------------------------------------------------
       //   Global Variables
       //----------------------------------------------------------------------
      /if not defined(QUSEC)
      /define QUSEC
      /include QSYSINC/QRPGLESRC,QUSEC
      /endif

       // Number of assertions called.
     D assertCalled    S             10I 0

       // Latest assertion failure event information. Can be blank if no
       //  assertion failure event since last assertion.
     D assertFailEvt   DS                  likeds(assert_failevent_t)


       //----------------------------------------------------------------------
       //   PROCEDURES
       //----------------------------------------------------------------------

     /**
      * \brief Assert equal on alpha values
      *
      * Assert equality between two alphanumeric values.
      *
      * \param Expected value
      * \param Actual value
      */ 
     P assert_assertEquals...
     P                 B                   export
     D                 PI
     D  expected                  32565A   const
     D  actual                    32565A   const

     D msg             S            256a

      /free
        msg = 'Expected ' + quote + %trimr(expected) + quote + ','
            + ' but was ' + quote + %trimr(actual  ) + quote + '.';
        assert_assertTrue( expected = actual : msg);
      /end-free
     P                 E


     /**
      * \brief Assert condition
      * 
      * Asserts that a condition is true. See prototype.
      * 
      * \param Condition
      * \param Message if false
      */
     P assert_assertTrue...
     P                 B                   export
     D                 PI
     D  condition                      N   const
     D  msgIfFalse                  256A   const
      /free
        assertCalled += 1;
        assert_clearFailEvent();

        if (not condition);
          assert_fail(msgIfFalse);
        endif;
      /end-free
     P                 E


     /**
      * \brief Clear latest assertion failure event
      * 
      * Clear the latest assertion failure event.
      */
     P assert_clearFailEvent...
     P                 B                   export
     D                 PI
      /free
        assertFailEvt = *blank;
      /end-free
     P                 e


     /**
      * \brief Fail 
      * 
      * Signals a test failure et stops the test.
      * 
      * \param Message
      */
     P assert_fail     B                   export
     D                 PI
     D  msg                         256A   const
      /free
        assertFailEvt.msg = msg;
        assertFailEvt.callStkEnt = getCallStack();

        sndEscapeMsg( msg : TWO_CALL_STK_LVL_ABOVE );
      /end-free
     P                 E


     /**
      * \brief Return assertions call count
      * 
      * Returns the number of time assertions were called.
      * 
      * \return Number of times assertions were called
      */
     P assert_getCalled...
     P                 B                   export
     D                 PI            10I 0
      /free
        return assertCalled;
      /end-free
     P                 e


     /**
      * \brief Return fail event 
      * 
      * Return information about the latest assertion failure event.
      * 
      * \brief Fail event info
      */
     P assert_getFailEvent...
     P                 B                   export
     D                 PI                  likeds(assert_failevent_t)
      /free
        return assertFailEvt;
      /end-free
     P                 E


     P getCallStack    B                   export
     D                 PI                  likeds(callStackentry_t)
     D                                     Dim(MAX_CALL_STK_SIZE)

       // Call stack entries.
     D callStkEnt      ds                  LikeDs(callstackentry_t)
     D                                     Dim(MAX_CALL_STK_SIZE)
       // Job id.
     D jobIdInfo       ds                  LikeDs(dsJIDF0100)
       // Call stack info header.
     D hdr             ds                  LikeDs(dsCSTK0100Hdr)
     D                                     Based(hdr_p)
     D hdr_p           s               *
       // Call stack info entry.
     D ent             ds                  LikeDs(dsCSTK0100Ent)
     D                                     Based(ent_p)
     D ent_p           s               *
       // Big buffer to receive call stack info.
     D rawCallStk      s           5000a
       // Statement Id.
     D sttId           s             10a   Based(sttId_p)
     D sttId_p         s               *
       // Procedure name buffer.
     D procNmBuffer_p  s               *
     D procNmBuffer    s            256a   Based(procNmBuffer_p)
       // Index.
     D i               s             10i 0

      /free

        jobIdInfo.jobNm = '*';
        jobIdInfo.usrNm = *blank;
        jobIdInfo.jobNb = *blank;
        jobIdInfo.intJobId = *blank;
        jobIdInfo.reserved = *loval;
        jobIdInfo.threadInd = 1;
        jobIdInfo.threadId  = *loval;

        clear QUSEC;

        QWVRCSTK( rawCallStk :
                  %size(rawCallStk) :
                  'CSTK0100' :
                  jobIdInfo :
                  'JIDF0100' :
                  QUSEC );

        hdr_p = %addr(rawCallStk);
        ent_p = hdr_p + hdr.callStkOff;

        // TODO Refactor the following piece of code.
        // Skip the first call stack entry (this procedure).
        ent_p += ent.len;
        // Skip the next call stack entry (the fail procedure).
        ent_p += ent.len;

        for i = 1 to (hdr.nbCallStkEntRtn - 2);    // TODO : min(header.nbCallStkEntRtn,MAX_CALL_STK

          callStkEnt(i).pgmNm = ent.pgmNm;
          callStkEnt(i).modNm = ent.modNm;

          if ent.procNmLen <> 0;
            procNmBuffer_p = ent_p + ent.dsplToProcNm;
            callStkEnt(i).procNm = %subst( procNmBuffer: 1: ent.procNmLen );
          else;
            callStkEnt(i).procNm = *blank;
          endif;

          if ent.nbSttId > 0;
            sttId_p = ent_p + ent.dsplToSttId;
            callStkEnt(i).specNb = sttId;
          else;
            callStkEnt(i).specNb = *blank;
          endif;

          ent_p += ent.len;

        endfor;

        return callStkEnt;

      /end-free
     P                 E


     /**
      * \brief Assert equality between integer values
      * 
      * Assert equality between two integers.
      * 
      * \param Expected value
      * \param Actual value
      */
     P assert_equalsInteger...
     P                 B                   export
     D                 PI
     D  expected                     10I 0 const
     D  actual                       10I 0 const

     D msg             s            256a

      /free
        msg = 'Expected ' + %char(expected) + ','
            + ' but was ' + %char(actual  ) + '.';

        assert_assertTrue( expected = actual : msg);
      /end-free
     P                 E


     P assert_clearCounter...
     P                 B                   export
      /free
       assertCalled = 0;
      /end-free
     P                 E
