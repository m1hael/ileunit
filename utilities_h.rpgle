      /if not defined (ILEUNIT_UTIL)
      /define ILEUNIT_UTIL
      
      //
      // Prototypes for USRSPC.
      //

       // Creates a User Space. Returns a pointer to it.
     D ileunit_createUserspace...
     D                 PR              *   extproc('ileunit_createUserspace')
        // Qualified user space name.
     D  usrSpc                             const likeds(object_t)
        // User space text description.
     D  text                         50A   const

      /endif
      